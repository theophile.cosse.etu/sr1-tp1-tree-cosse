package com.theophilecosse;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import com.theophilecosse.client.FTPClientCreationException;
import com.theophilecosse.client.FTPClient;

import org.junit.Test;

/**
 * Unit test for the Test class using SimpleTree with FTPClient
 * 
 */
public class AppTest 
{
    @Test(expected = FTPClientCreationException.class)
    public void whenFTPClientCreatedWithWrongIP_thenExceptionIsThrown() throws FTPClientCreationException, IOException {
        FTPClient ftpClient = new FTPClient("ftpbuntu.cm", 21); 
    }
}
