package com.theophilecosse.tree;

import java.io.IOException;

import com.theophilecosse.client.FTPClient;

import org.junit.Test;

/**
 * Unit test for the Test class using SimpleTree with FTPClient
 * 
 */
public class SimpleTreeTest
{
    /**
     * We create an FTPClient without loggin in to make sur sure the list function doesn't work
     * and SimpleTree return the desired exception
     * 
     * @throws TreeListingException
     * @throws IOException
     */
    @Test(expected = TreeListingException.class)
    public void whenUsingIncorrectListCLient_thenExceptionIsThrown() throws TreeListingException, Exception{
        FTPClient ftpClient = new FTPClient("ftp.ubuntu.com", 21);
        SimpleTree simpleTree = new SimpleTree(ftpClient);
        simpleTree.tree();
    }
}
