package com.theophilecosse;

import java.io.IOException;

import com.theophilecosse.client.FTPClient;
import com.theophilecosse.client.FTPClientCreationException;
import com.theophilecosse.tree.SimpleTree;
import com.theophilecosse.tree.TreeListingException;

/**
 * Test class to use SimpleTree with FTPClient
 *
 */
public class App {
    public static void main(String[] args) {
        if (args.length < 1){
            System.err.println("Usage: java -jar rs1-tp1-tree-cosse-1.0-SNAPSHOT.jar ip port username password");
            System.exit(-1);
        }
        FTPClient ftpClient;
        try {
            if (args.length >= 2) {
                ftpClient = new FTPClient(args[0], Integer.parseInt(args[1]));
            } else {
                ftpClient = new FTPClient(args[0], 21);
            } 
            if (args.length == 4){
                ftpClient.login(args[2], args[3]);
            } else {
                ftpClient.login("anonymous","aaa");
            }
            SimpleTree simpleTree = new SimpleTree(ftpClient);

            simpleTree.tree();
            
            ftpClient.closeConnection();

        } catch (TreeListingException e) {
            System.err.println("Error while listing files");
        } catch (FTPClientCreationException e) {
            System.err.println("unable to create FTP Client");
        }
        catch (IOException e) {
            System.err.println("Error while sending/receiving data from ftp server");
        }
        catch (Exception e){
            System.err.println("Unknown error");
            e.printStackTrace();            
        }
    }
}
