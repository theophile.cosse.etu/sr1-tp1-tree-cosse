package com.theophilecosse.tree;

import java.util.ArrayList;
/**
 * Listable
 * 
 * This interface is used by any Tree API such as SimpleTree and is implement by any class
 * who can list files in the same fashion as the linux command "ls -l"
 * 
 */
public interface Listable {
    /**
     * Return the list of the files and directory in the specified directory
     * 
     * @param directory
     * @return An array list of strings containing name type and right of the file / directory
     * @throws Exception
     */
    public ArrayList<String> list(String directory) throws Exception;
}