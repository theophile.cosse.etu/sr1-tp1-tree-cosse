package com.theophilecosse.tree;

import java.util.ArrayList;
import com.theophilecosse.tree.TreeListingException;

public class SimpleTree {

    private Listable listable;

    public SimpleTree(Listable listable) {
        this.listable = listable;
    }

    public void tree(String directory ,String prefix) throws TreeListingException{
        try {
            ArrayList<String> data  = listable.list(directory);
            for (int i = 0; i < data.size(); i++) {
                String str = data.get(i);
                boolean isDir = str.startsWith("d");
                String[] tmpSplit = str.split("\\s+");
                String name = tmpSplit[tmpSplit.length-1];
    
                if (name.startsWith(".")){
                    continue;
                }
                
                if (i == (data.size()-1)){ // last file of the directory
                    System.out.println(prefix + "└── " + name);
                    if (isDir){
                        tree(directory+"/"+name, prefix + "\t");
                    }
                } else {
                    System.out.println(prefix + "├── " + name);
                    if (isDir){
                        tree(directory+"/"+name, prefix + "|\t");
                    }
                }
            }            
        } catch (Exception e) {
            throw new TreeListingException("Error while listing files", e) ; 
        }

    }

    public void tree() throws Exception{
        tree(".","");
    }
   
}