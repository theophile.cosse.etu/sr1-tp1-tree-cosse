package com.theophilecosse.tree;

/**
 * TreeListingException
 * 
 * This exception is thrown by Tree classes when failling to list
 * 
 */
public class TreeListingException extends RuntimeException {
    public TreeListingException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }    
}