package com.theophilecosse.client;

/**
 * FTPClientCreationException
 * 
 * This exception is thrown by FTPClient when failling to connect to the FTP server
 * 
 */
public class LoginException extends RuntimeException {
    public LoginException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }    
}