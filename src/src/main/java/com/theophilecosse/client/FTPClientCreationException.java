package com.theophilecosse.client;

/**
 * FTPClientCreationException
 * 
 * This exception is thrown by FTPClient when he fail to initiate his controle socket
 * 
 */
public class FTPClientCreationException extends RuntimeException {

    public FTPClientCreationException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }    
}