package com.theophilecosse.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.logging.Handler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import com.theophilecosse.tree.Listable;

/**
 * FTPClient
 * 
 * This class is allow to communicate with an ftp server to perform listing
 * For sake of greater clarity only the passive mode is used (PASV)
 * 
 * To use this class you have to first call the login function, you
 * can them call the list function.
 * 
 */
public class FTPClient implements Listable{

    private Socket controlSocket;
    private PrintWriter controlWriter;
    private BufferedReader controlReader;
    private Socket dataSocket;
    private PrintWriter dataWriter;
    private BufferedReader dataReader;

    static Logger logger = Logger.getLogger("logger");
    static Handler fileHandler;

    /**
     * Create an FTPClient using the specified ip and port
     * 
     * @param ip IP of the FTP server
     * @param port Port of the FTP server
     * @throws FTPClientCreationException
     * @throws IOException
     */
    public FTPClient(String ip, int port) throws FTPClientCreationException, IOException {
        fileHandler = new FileHandler("logger.log", 10000, 10);
        logger.setLevel(Level.ALL);
        logger.addHandler(fileHandler);
        logger.setUseParentHandlers(false);
        try {
            controlSocket = new Socket(ip, port);
        } catch (IOException e) {
            throw new FTPClientCreationException("Incorrect port / ip", e) ;          
        }
        
        controlWriter = new PrintWriter(controlSocket.getOutputStream(), true);
        controlReader = new BufferedReader(new InputStreamReader(controlSocket.getInputStream()));
        logger.log(Level.INFO,controlReader.readLine());
    }

    /**
     * Create the data socket using the passive mode (PASV)
     * 
     * @throws DataChannelException
     * @throws IOException
     */
    public void openDataChannel() throws DataChannelException, IOException{

        this.sendCommand("PASV");
        String pasvR = controlReader.readLine();
        String[] pasvSplit = pasvR.substring(pasvR.indexOf("(")+1,pasvR.indexOf(")")).split(",");
        int[] pasvInt = new int[6];
        for (int i=0;i<6;i++){
            pasvInt[i] = Integer.valueOf(pasvSplit[i]);
        }
        String dataIP = pasvSplit[0] + "." + pasvSplit[1] + "." + pasvSplit[2] + "." + pasvSplit[3];
        int dataPort = (pasvInt[4]*256) + pasvInt[5];
        try {
            dataSocket = new Socket(dataIP,dataPort);
        } catch (RuntimeException e) {
            throw new DataChannelException("Can't open data channel", e) ;          
        }
        dataWriter = new PrintWriter(dataSocket.getOutputStream(), true);
        dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));      
    }

    /**
     * Open a data channel with the server then
     * return the list of the files and directory in the specified directory
     * 
     * @param directory
     * @return An array list of strings containing name type and right of the file / directory
     * @throws DataChannelException
     * @throws IOException
     */
    @Override
    public ArrayList<String> list(String directory) throws DataChannelException, IOException{
        openDataChannel(); // Opening a new data channel each time list is called is mandatory
        this.sendCommand("LIST", directory);
        logger.log(Level.INFO,controlReader.readLine());  
        ArrayList<String> files = new ArrayList<String>();
        String file;
        while((file = dataReader.readLine()) != null) // The end of the listing is signaled by an empty line.
        {
          files.add(file);
        }
        logger.log(Level.INFO,controlReader.readLine()); // Log the server response
        return files;
    }

    
    /**
     * Send the specified command to the FTP server with the specified arguments
     * A StringBuilder is used to create the command and append the Telnet EOF
     * 
     * @param command
     * @param arguments
     */
    public void sendCommand(String command,String... arguments){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(command);
        for (String argument : arguments) {
            stringBuilder.append(' ');
            stringBuilder.append(argument);
        }
        stringBuilder.append("\r\n");
        controlWriter.print(stringBuilder.toString());
        controlWriter.flush();
    }


    /**
     * Log the user to the FTP Server using only his username
     * @param username
     * @throws LoginException
     */
    public void login(String username) throws LoginException {
        try {
            this.sendCommand("USER",username);
            logger.log(Level.INFO,controlReader.readLine());            
        } catch (IOException e) {
            throw new LoginException("Error while logging with username only", e) ; 
        }

    }
    /**
     * Log the user to the FTP Server using username and password
     * @param username
     * @throws LoginException
     */
    public void login(String username, String password) throws LoginException {
        try {
            this.sendCommand("USER",username);
            logger.log(Level.INFO,controlReader.readLine());
            this.sendCommand("PASS",password);
            logger.log(Level.INFO,controlReader.readLine());            
        } catch (IOException e) {
            throw new LoginException("Error while logging with username and password", e) ; 
        }   
    }
    /**
     * Close the connection between the FTP client and the server 
     * @throws IOException
     */
    public void closeConnection() throws IOException{
        dataSocket.close();
        controlSocket.close();
    }
    
}