package com.theophilecosse.client;

/**
 * FTPClientCreationException
 * 
 * This exception is thrown by FTPClient when failing to create Data socket
 * 
 */
public class DataChannelException extends RuntimeException {
    public DataChannelException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }    
}