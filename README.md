# Application «Tree FTP» 
Théophile Cosse, 03/02/2021

## Introduction 

Cette application est adaptation de la commande [tree](https://linux.die.net/man/1/tree) pour serveur FTP.

## Architecture 

Le code est diviser en deux partie distinct :
 * La partie **Réseaux** utilisant la classe ClientFTP.
 * La partie **Tree** utilisant la classe SimpleTree.

La classe SimpleTree utilise une instance d'un object implémentant l'interface **Listable**.
Ainsi le code de SimpleTree peut etre re-utiliser avec d'autre protocole que FTP.

Le but du projet étant d'utiliser le protocole FTP **SimpleTree** n'est actuellement utiliser avec la classe **FTPClient**.

Une classe **App** contenant le main est également présente dans le projet, cette classe parse les argument du main, instancie **FTPClient**,
effectue un appel a la fonction **login** puis **SimpleTree** et enfin appel **SimpleTree.login**.

Les exceptions suivante sont emise par la classe FTPClient :
 * FTPClientCreationException, émise en cas d'impossibilité de creer les sockets necessaires.
 * LoginException, émise en cas d'erreur lors du login
 * DataChannelException, émise si une erreur survient lors de l'établissement de canal donnée.

La classe SimpleTree emet l'exception TreeListingException en cas d'erreur lors du listing.

Un logger est utilisé pour enregistré toute les réponses du serveur FTP.

## Code Samples 

La fonction list de la classe FTPClient, chargé de listé les fichier présents dans le répertoire spécifié
en argument :
```java
public ArrayList<String> list(String directory) throws DataChannelException, IOException{
    openDataChannel(); // Opening a new data channel each time list is called is mandatory
    this.sendCommand("LIST", directory);
    logger.log(Level.INFO,controlReader.readLine());  
    ArrayList<String> files = new ArrayList<String>();
    String file;
    while((file = dataReader.readLine()) != null) // The end of the listing is signaled by an empty line.
    {
        files.add(file);
    }
    logger.log(Level.INFO,controlReader.readLine()); // Log the server response
    return files;
}
```

La fonction sendCommand de la classe FTPClient permet d'envoyer de maniére simple une commande au serveur FTP :
```java
public void sendCommand(String command,String... arguments){
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(command);
    for (String argument : arguments) {
        stringBuilder.append(' ');
        stringBuilder.append(argument);
    }
    stringBuilder.append("\r\n");
    controlWriter.print(stringBuilder.toString());
    controlWriter.flush();
}
```

L'algorithme utilisé pour identifier si un fichier est un dossier et quel est son nom :
```java
String str = data.get(i);
boolean isDir = str.startsWith("d");
String[] tmpSplit = str.split("\\s+"); 
String name = tmpSplit[tmpSplit.length-1];
```

Paramétrage du Logger :
```java
fileHandler = new FileHandler("logger.log", 10000, 10);
logger.setLevel(Level.ALL);
logger.addHandler(fileHandler);
logger.setUseParentHandlers(false);
```

Algorithme recursif de la commande tree :
```java
if (i == (data.size()-1)){ // last file of the directory
    System.out.println(prefix + "└── " + name);
    if (isDir){
        tree(directory+"/"+name, prefix + "\t");
    }
} else {
    System.out.println(prefix + "├── " + name);
    if (isDir){
        tree(directory+"/"+name, prefix + "|\t");
    }
}
```




